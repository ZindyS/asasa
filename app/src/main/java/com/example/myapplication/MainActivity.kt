package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.myapplication.databinding.ActivityMainBinding



//Переменная
val adada = "asdas"

//Этот класс описывает поведение экрана Main
//02.04.2023
//Роман
class MainActivity : AppCompatActivity() {
    lateinit var activity : ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity = ActivityMainBinding.inflate(layoutInflater)
        setContentView(activity.root)

        activity.textView.text = "ASDASDA"
    }
}